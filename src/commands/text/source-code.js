'use strict';

const commando = require('discord.js-commando');
const oneLine = require('common-tags').oneLine;
const _ = require('lodash');

module.exports = class SourceCommand extends commando.Command {
  constructor(client) {
    super(client, {
      name: 'source-code',
      group: 'text',
      memberName: 'source-code',
      description: 'Share the source code of the bot',
      details: oneLine`
        Find where to contribute to your favorite meme machine!
			`
    });
  }

  async run(msg, args) {
    let source = "https://gitlab.com/pixelenthusiasts/bot";
    return msg.reply(source);
  }
};